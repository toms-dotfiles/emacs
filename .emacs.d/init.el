;; Load the rest of the packages
(setq package-enable-at-startup nil)

;; load literal configuration
(org-babel-load-file
  (expand-file-name "config.org" user-emacs-directory))

;; additional configuration

;; not sure what the below does, so disabling it
;; this could be the kind of strange file finder
  ;;  (setq gc-cons-threshold 402653184
  ;;    gc-cons-percentage 0.6
  ;;    file-name-handler-alist-original file-name-handler-alist
  ;;    file-name-handler-alist nil
  ;;    site-run-file nil)
  ;;  (add-hook 'emacs-startup-hook
  ;;    (lambda ()
  ;;      (setq
  ;;        gc-cons-threshold 20000000
  ;;        gc-cons-percentage 0.1
  ;;        file-name-handler-alist file-name-handler-alist-original)
  ;;      (makunbound 'file-name-handler-alist-original)))
  ;;  (add-hook 'minibuffer-setup-hook (lambda () (setq gc-cons-threshold 40000000)))
  ;;  (add-hook 'minibuffer-exit-hook (lambda ()
  ;;    (garbage-collect)
  ;;    (setq gc-cons-threshold 20000000)))

;; Dump custom-set-variables to a garbage file and don't load it
(setq custom-file "~/.emacs.d/custom-set-variables.el")
(provide 'init)
;;; init.el ends here
